import {useState} from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faBed, faCalendar, faCar, faPerson, faPlane, faTaxi} from '@fortawesome/free-solid-svg-icons';
import './Header.css';
import {DateRange} from 'react-date-range';
import 'react-date-range/dist/styles.css'; // main css file
import 'react-date-range/dist/theme/default.css'; // theme css file
import {format} from "date-fns"

const Header = ({type}) => {
   // DEFINE THE BOLEEN FOR ONCLICK FUNCTION
   const [openOptionDate, setOpenOptioDate] = useState(false);
   const [openOptionPerson, setOpenOptioPerson] = useState(false);
   
   // DEFINE THE DATE FORMAT 
   const [date, setDate] = useState([
         {
            startDate: new Date(),
            endDate: new Date(),
            key: "selection"
         }
   ])

   // DEFINE THE STATE FOO OPTION 
   const  [option, setOption] = useState({
      adult:1,
      children:0,
      room:1,

   });
   // console.log(option--)

   // DEFIE THE FUNCTION HANDEL OPTIOM 
   const  handleOption  = (name , operation) =>{
     
      setOption((prev) =>{
         // console.lojjjjg(...prev,  );
         return{
            ...prev, [name]: operation ==='i' ? option[name] + 1: option[name]- 1,
         }
      })
   }

  return (
    <div className='header-wrapper'>
        <div  className={type === "list"? "header-container list-mode" : "header-container" }>
            <div className="header-list">
                    <div className="header-list-item active">
                        <FontAwesomeIcon icon={faBed} />
                        <span>Flights </span>
                     </div>

                     <div className="header-list-item">
                        <FontAwesomeIcon icon={faPlane} />
                        <span>Car rentals</span>
                     </div>

                     <div className="header-list-item">
                        <FontAwesomeIcon icon={faCar} />
                        <span>Attraction</span>
                     </div>

                     <div className="header-list-item">
                        <FontAwesomeIcon icon={faBed} />
                        <span>Stays</span>
                     </div>

                     <div className="header-list-item">
                        <FontAwesomeIcon icon={faTaxi} />
                        <span>Airport taxis</span>
                     </div>  
            </div>
            {
               type !== "list" && 
             <>
      
            <h1 className='header-title'>
                      A liftetime of discounts? It is Genius.
                     </h1>
                     <p className='headerDesc'>Get rewared for your travel - unlock instant savings of 10% with free a VhongBooking account</p>
                     <button className ="header-button">Sign in / Register</button>

                     <div className="header-search">
                         <div className="header-search-item">
                              <FontAwesomeIcon icon={faBed} className="header-icon"/>
                              <input type="text" placeholder="Where are you go" className='header-search-input'/>
                         </div>

                         <div className="header-search-item">
                              <FontAwesomeIcon icon={faCalendar} className="header-icon"/>
                              <span onClick ={( ) => setOpenOptioDate(!openOptionDate)}className='header-search-text'>{`${format(date[0].startDate, "MM/dd/yyyy")} to ${format(date[0].endDate, "MM/dd/yyyy")} `}</span>
                              {openOptionDate && 
                              <DateRange editableDateInputs={true}
                                    onChange={item => setDate([item.selection])}
                                    moveRangeOnFirstSelection={false}
                                    ranges={date}
                                    className="date"
                              />
                                 }
                         </div>

                         <div className="header-search-item">
                              <FontAwesomeIcon icon={faPerson} className="header-icon"/>
                              <span onClick ={( ) =>  setOpenOptioPerson(!openOptionPerson)} className='header-search-text'>{`${option.adult} adult ${option.children} children ${option.room}`} </span>
                              {openOptionPerson && 
                              <div className="options">
                                    <div className="option-item">
                                       <span className="option-text">
                                          Adult
                                       </span>
                                       <div className="option-counter">

                                                <button className="option-counter-button" onClick={() => handleOption("adult", "d")} disabled={option.adult <= 1}>
                                                      -
                                                </button>
                                                <span className='option-counter-number'>
                                                      {option.adult}
                                                </span>
                                                <button className="option-counter-button" onClick={() => handleOption("adult", "i")}>
                                                      +
                                                </button>
                                       </div>

                                    </div>

                                    <div className="option-item">
                                       <span className="option-text">
                                          Children
                                       </span>
                                       <div className="option-counter">
                                                <button className="option-counter-button" onClick={() => handleOption("children", "d")} disabled={option.children <= 1}>
                                                      -
                                                </button>
                                                <span className='option-counter-number'>
                                                      {option.children}
                                                </span>
                                                <button className="option-counter-button" onClick={() => handleOption("children", "i")}>
                                                      +
                                                </button>
                                       </div>
                                    </div>

                                    <div className="option-item">
                                       <span className="option-text">
                                          Room
                                       </span>
                                       <div className="option-counter">
                                                <button className="option-counter-button" onClick={() => handleOption("room", "d")} disabled={option.room <= 1}>
                                                      -
                                                </button>
                                                <span className='option-counter-number'>
                                                      {option.room}
                                                </span>
                                                <button className="option-counter-button"  onClick={() => handleOption("room", "i")}>
                                                      +
                                                </button>
                                       </div>
                                    </div>

                              </div>
                                    }
                         </div>

                         <div className="header-search-item">
                              <button className="header-button">
                                 Search
                              </button>
                         </div>

                  
                   </div>
                   </>
              }
            </div>   
            
    </div>
  )
}

export default Header