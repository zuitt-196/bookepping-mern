import React from 'react'
import './Navbar.css'
const Navbar = () => {
  return (
    <div className="navbar-wrapper">
        <div className="navabar-container">
                <span className='navbar-logo'>VhongBookiping</span>

                <div className="navbar-items">
                    <button className="navbar-register-button navbar-button">Register</button>
                    <button className="navbar-login-button navbar-button">Login</button>
                </div>
        </div>
    </div>
  )
}

export default Navbar